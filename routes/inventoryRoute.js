// src/routes/inventoryRoutes.js
const express = require('express');
const router = express.Router();
const inventoryController = require('../controllers/inventoryController');
//const authMiddleware = require('../middleware/authMiddleware');

router.post('/',inventoryController.initializeInventory);
module.exports = router;
// Get details of a specific inventory route (requires authentication)
// router.get('/inventory/:id', authMiddleware, async (req, res) => {
//   try {
//     const inventoryDetails = await inventoryController.getInventoryDetails(req.params.id);
//     res.status(200).json({ inventoryDetails });
//   } catch (error) {
//     console.error('Error in GET /inventory/:id:', error);
//     res.status(500).json({ error: 'Internal Server Error' });
//   }
// });

