// routes/restaurantRoute.js
const express = require('express');
const router = express.Router();
const restaurantsController = require('../controllers/restaurantsController');

// Restaurant listing route
router.get('/', restaurantsController.getRestaurants);

// Restaurant details route
router.get('/:id', restaurantsController.getRestaurantDetails);

module.exports = router;
